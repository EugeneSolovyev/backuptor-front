# Backuptor App

## Installation

```
$ npm install
$ npm start
```

* Copy .env.example file and rename to '.env';
* Change SERVER_API variable;

## Build for Production

```
$ npm run build
```

## Clear bundle
```
$ npm run clear
```