import React from 'react';
import {
	Button,
	Icon
} from 'antd';
import './style.styl';

class HeaderComponent extends React.Component {
	constructor(props) {
		super(props);
		this.openDrawer = this.openDrawer.bind(this);
	}

	openDrawer() {
		this.props.ToggleDrawer();
	}

	render() {
		return (
			<div 
				className="backuptor-header">
				<h1 
					className="backuptor-header__title">Backuptor</h1>
				<Button 
					onClick={this.openDrawer} 
					style={{ marginLeft: 16 }}>
					<Icon 
						type='appstore-o' />
				</Button>
			</div>
		);
	}
}

export default HeaderComponent;