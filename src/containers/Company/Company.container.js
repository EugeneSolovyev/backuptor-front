import ReduxHOC from '../_ContainerWrapper';
import Company from './Company.component';
import * as Actions from './Company.actions';

const mapStateToProps = (state) => ({
	...state.Company,
	masters: state.Home.masters,
});

export default ReduxHOC(Company, mapStateToProps, Actions);