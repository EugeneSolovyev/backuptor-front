import React from 'react';
import {
	Form,
	Icon,
	Input,
	Button,
	message,
	Select,
} from 'antd';
import './style.styl'

class Company extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			masterId: '',
			...this.props
		};
		this.changeField = this.changeField.bind(this);
		this.handleMaster = this.handleMaster.bind(this);
		this.registerCompany = this.registerCompany.bind(this);
	}

	changeField(e) {
		this.setState({
			[e.target.name]: e.target.value,
		})
	}

	handleMaster(value) {
		this.setState({
			masterId: value,
		});
	}

	registerCompany() {
		let {
			companyPath,
			companyName,
			containerName,
			containerDbName,
			containerDbUser,
			containerDbPassword,
			containerDbPort,
			masterId,

		} = this.state;

		if (!companyName || !companyPath 
			|| !containerName || !containerDbName 
			|| !containerDbUser || !containerDbPassword 
			|| !containerDbPort || !masterId) return message.warning('Please fill all fields');

		this.props.RegisterCompany({
			companyPath,
			companyName,
			containerName,
			containerDbName,
			containerDbUser,
			containerDbPassword,
			containerDbPort,
			masterId,
		})
	}

	render() {
		let {
			companyPath,
			companyName,
			containerName,
			containerDbName,
			containerDbUser,
			containerDbPassword,
			containerDbPort,
		} = this.state;
		
		return (
			<Form className='backuptor-register-company'>
				<Form.Item>
					<Input
						prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Company name'
						name='companyName'
						value={companyName} 
						onChange={this.changeField} />
				</Form.Item>
				<Form.Item>
					<Input
						prefix={<Icon type="folder" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Company path'
						name='companyPath'
						value={companyPath} 
						onChange={this.changeField} />
				</Form.Item>
				<Form.Item>
					<Input
						prefix={<Icon type="home" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Container name'
						name='containerName'
						value={containerName} 
						onChange={this.changeField} />
				</Form.Item>
				<Form.Item>
					<Input
						prefix={<Icon type="book" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Container database name'
						name='containerDbName'
						value={containerDbName} 
						onChange={this.changeField} />
				</Form.Item>
				<Form.Item>
					<Input
						prefix={<Icon type="database" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Container database user'
						name='containerDbUser'
						value={containerDbUser} 
						onChange={this.changeField} />
				</Form.Item>
				<Form.Item>
					<Input
						prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Container database password'
						type='password'
						name='containerDbPassword'
						value={containerDbPassword} 
						onChange={this.changeField} />
				</Form.Item>
				<Form.Item>
					<Input
						prefix={<Icon type="link" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Container database port'
						name='containerDbPort'
						value={containerDbPort} 
						onChange={this.changeField} />
				</Form.Item>
				<Select 
					placeholder='Select master'
					name='masterId'
					onChange={this.handleMaster}>
					{this.props.masters.map((item, key) => (
						<Select.Option 
							key={key}
							value={item.id}>
							{item.name}
						</Select.Option>
					))}
				</Select>
				<Form.Item>
					<Button 
						block 
						type='primary' 
						onClick={this.registerCompany}>
						Register Company
					</Button>
				</Form.Item>
			</Form>
		);
	}
}

export default Company;