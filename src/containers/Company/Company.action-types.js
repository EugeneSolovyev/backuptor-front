const CompanyActionTypes = {
	COMPANY_CREATED_SUCCESSFULLY: 'COMPANY_CREATED_SUCCESSFULLY',
	COMPANY_CREATED_ERROR: 'COMPANY_CREATED_ERROR',
};

export default CompanyActionTypes;