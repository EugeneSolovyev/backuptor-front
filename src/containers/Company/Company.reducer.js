import CompanyActionTypes from './Company.action-types';

const initialState = {
	masterId: 1,
	companyPath: '',
	companyName: '',
	containerName: '',
	containerDbName: '',
	containerDbUser: '',
	containerDbPassword: '',
	containerDbPort: ''
};

export default (state = initialState, action) => {
	switch (action.type) {
		case CompanyActionTypes.COMPANY_CREATED_SUCCESSFULLY:
			state.masterId++;
			return state;
		default:
			return state;
	}
}