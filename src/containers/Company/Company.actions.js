import { message } from 'antd';
import { FetchingPost } from '../../API/api';
import API_URL from '../../API/api.constants';
import CompanyActionTypes from './Company.action-types';

const RegisterCompany = (state) => {
	return dispatch => FetchingPost(API_URL.RegisterCompany, state)
		.then(response => {
			message.success('Company is created', 2.5);
			dispatch({
				type: CompanyActionTypes.COMPANY_CREATED_SUCCESSFULLY,
				payload: response,
			});
		})
		.catch(error => {
			message.error(error.message, 2.5);
			dispatch({
				type: CompanyActionTypes.COMPANY_CREATED_ERROR,
			});
		});
};

export {
	RegisterCompany,
}