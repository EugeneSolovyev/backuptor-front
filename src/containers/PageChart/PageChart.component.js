import React from 'react';
import PropTypes from 'prop-types';
import { ResponsiveStream } from '@nivo/stream';
import { Card } from 'antd';

import './style.styl';

class StreamChart extends React.Component {
	render() {
		// Wierd bug with <ResponsiveStream /> in keys attribute.
		// We are forced to use _.cloneDeep, otherwise stream chart wouldn't rendered.
		// TODO: report bout this bug: https://github.com/plouc/nivo
		let { title, chartData, Keys } = _.cloneDeep(this.props);

		return (
			<div className="page-chart">
				<Card title={title}>
					<ResponsiveStream
						data={chartData}
						keys={Keys}
						margin={{
							top: 50,
							right: 110,
							bottom: 50,
							left: 60
						}}
						axisBottom={{
							orient: 'bottom',
							tickSize: 5,
							tickPadding: 5,
							tickRotation: 0,
							legend: '',
							legendOffset: 36,
						}}
						axisLeft={{
							orient: 'left',
							tickSize: 5,
							tickPadding: 5,
							tickRotation: 0,
							legend: '',
							legendOffset: -40
						}}
						enableGridY={true}
						curve="linear"
						offsetType="none"
						fillOpacity={0.85}
						borderColor="#000"
						motionStiffness={90}
						motionDamping={15}
						colors="accent"
						legends={[
							{
								anchor: 'bottom-right',
								direction: 'column',
								translateX: 100,
								itemWidth: 80,
								itemHeight: 20,
								itemTextColor: '#999',
								symbolSize: 12,
								symbolShape: 'circle',
								effects: [
									{
										on: 'hover',
										style: {
											itemTextColor: '#000'
										}
									}
								]
							}
						]}
						tooltipFormat={(data) => `${data}%`}
					/>
				</Card>
			</div>
		);
	}
}

export default StreamChart;
