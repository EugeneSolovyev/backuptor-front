import * as Actions from './Master.actions';
import Master from './Master.component';
import ReduxHOC from '../_ContainerWrapper';

const mapStateToProps = (state) => ({
	...state.Master,
	...state.Home
});

export default ReduxHOC(Master, mapStateToProps, Actions);