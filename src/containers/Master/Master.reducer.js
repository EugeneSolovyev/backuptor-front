import MasterActionTypes from './Master.action-types';

const initialState = {
	name: '',
	ip: '',
};

export default (state = initialState, action) => {
	switch (action.type) {
		case MasterActionTypes.MASTER_CREATED_FAILED:
			state = action.payload;
			return state;
		default:
			return state;
	}
};