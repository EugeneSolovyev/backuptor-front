import React from 'react';
import {
	Form,
	Icon,
	Input,
	Button,
	message,
} from 'antd';
import './style.styl';

class Master extends React.Component {
	constructor(props) {
		super(props);
		this.state = this.props;
		this.changeField = this.changeField.bind(this);
		this.registerMaster = this.registerMaster.bind(this);
	}

	changeField(e) {
		this.setState({
			[e.target.id]: e.target.value,
		})
	}

	registerMaster() {
		let {
			name,
			ip,
		} = this.state;
		
		if (!name || !ip) return message.warning('Please fill all fields');

		this.props.RegisterMaster({
			name,
			ip,
			slaves: this.props.slaves
		})
	}

	render() {
		let {
			name,
			ip,
		} = this.state;

		return (
			<Form className='backuptor-register-master'>
				<Form.Item>
					<Input
						prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Master name'
						id='name'
						value={name} 
						onChange={this.changeField} />
				</Form.Item>
				<Form.Item>
					<Input
						prefix={<Icon type="database" style={{ color: 'rgba(0,0,0,.25)' }} />}
						placeholder='Master IP'
						id='ip'
						value={ip} 
						onChange={this.changeField} />
				</Form.Item>
				<Form.Item>
					<Button 
						block 
						type='primary' 
						onClick={this.registerMaster}>
						Register Master
					</Button>
				</Form.Item>
			</Form>
		);
	}
}

export default Master;