import MasterActionTypes from './Master.action-types';
import { message } from 'antd';
import { FetchingPost } from '../../API/api';
import API_URL from '../../API/api.constants';

const RegisterMaster = (state) => {
	return (dispatch) =>
		FetchingPost(API_URL.RegisterMaster, state)
			.then((response) => {
				message.success('Master is created', 2.5);
				dispatch({
					type: MasterActionTypes.MASTER_CREATED_SUCCESSFULLY,
					payload: state
				});
			})
			.catch((error) => {
				message.error(error.message, 2.5);
				dispatch({
					type: MasterActionTypes.MASTER_CREATED_FAILED
				});
			});
};

export { RegisterMaster };
