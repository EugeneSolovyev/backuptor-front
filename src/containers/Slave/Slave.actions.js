import {
	message
} from 'antd';
import API_URL from '../../API/api.constants';
import {FetchingPost} from '../../API/api';
import SlaveActionTypes from './Slave.action-types';

const RegisterSlave = (state) => {
	return dispatch => FetchingPost(API_URL.RegisterSlave, state)
		.then(response => {
			message.success('Slave is created', 2.5);
			dispatch({
				type: SlaveActionTypes.SLAVE_CREATED_SUCCESSFULLY,
				payload: state,
			})
		})
		.catch(error => {
			message.error(error.message, 2.5);
			dispatch({
				type: SlaveActionTypes.SLAVE_CREATED_ERROR,
				payload: state,
			});
		});
}

export {
	RegisterSlave,
}