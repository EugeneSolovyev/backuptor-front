import * as Actions from './Slave.actions';
import Slave from './Slave.component';
import ReduxHOC from '../_ContainerWrapper';

const mapStateToProps = (state) => ({
	...state.Slave,
});

export default ReduxHOC(Slave, mapStateToProps, Actions);