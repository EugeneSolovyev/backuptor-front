import React from 'react';
import { Drawer, Icon } from 'antd';
import { Link } from 'react-router-dom';

import { COMMON_MENU } from './contants';
import './menu-component.styl';

class MenuComponent extends React.Component {
	constructor(props) {
		super(props);
		this.state = this.props;
		this.closeNav = this.closeNav.bind(this);
	}

	closeNav() {
		this.props.ToggleDrawer();
	}

	render() {
		const MenuCustom = COMMON_MENU.map((menuItem) => (
			<Link className="backuptor-menu__item" onClick={this.closeNav} to={menuItem.route} key={menuItem.key}>
				<Icon type={menuItem.icon} theme="outlined" /> {menuItem.name}
			</Link>
		));
		return (
			<div className="backuptor-menu">
				<Drawer
					title="Backuptor Navigation"
					placement="left"
					onClose={this.closeNav}
					width={350}
					visible={this.props.menuOpen}
				>
					{MenuCustom}
				</Drawer>
			</div>
		);
	}
}

export default MenuComponent;
