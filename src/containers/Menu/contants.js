const COMMON_MENU = [
	{
		key: '1',
		icon: 'dashboard',
		name: 'Dashboard',
		route: '/',
	},
	{
		key: '2',
		icon: 'folder-add',
		name: 'Registration of Master',
		route: '/register-master',
	},
	{
		key: '3',
		icon: 'folder-add',
		name: 'Registration of Slave',
		route: '/register-slave',
	},
	{
		key: '4',
		icon: 'user',
		name: 'Registration of Company',
		route: '/register-company',
	}
];

export {
	COMMON_MENU,
};