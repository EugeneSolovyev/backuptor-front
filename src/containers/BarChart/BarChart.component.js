import React from 'react';
import { Card } from 'antd';
import { ResponsiveBar } from '@nivo/bar';
import '../PageChart/style.styl';

class BarChart extends React.Component {
	render() {
		let { chartData, Keys, title, legendType } = this.props;

		return (
			<div className="page-chart">
				<Card title={title}>
					<ResponsiveBar
						data={chartData}
						keys={Keys}
						margin={{
							top: 50,
							right: 100,
							bottom: 50,
							left: 79
						}}
						indexBy="name"
						padding={0.3}
						groupMode="stacked"
						layout="horizontal"
						colors= {["#FF1493", "#7FFF00"]}
						colorBy="id"
						borderColor="inherit:darker(1.6)"
						axisBottom={{
							orient: 'bottom',
							tickSize: 5,
							tickPadding: 5,
							tickRotation: 0,
							legend: legendType,
							legendPosition: 'center',
							legendOffset: 36
						}}
						axisLeft={{
							orient: 'left',
							tickSize: 5,
							tickPadding: 0,
							tickRotation: 0,
							legend: '',
							legendPosition: 'center',
							legendOffset: -40
						}}
						enableGridX={true}
						labelSkipWidth={12}
						labelSkipHeight={12}
						labelTextColor="inherit:darker(1.6)"
						animate={true}
						motionStiffness={90}
						motionDamping={15}
            tooltip={function(data){
            	return `Total: ${data.data.total};  ${data.id} - ${data.value}%`;
            }}
						legends={[
							{
								dataFrom: 'keys',
								anchor: 'bottom-right',
								direction: 'column',
								justify: false,
								translateX: 120,
								translateY: 0,
								itemsSpacing: 2,
								itemWidth: 100,
								itemHeight: 20,
								itemDirection: 'left-to-right',
								itemOpacity: 0.85,
								symbolSize: 20,
								effects: [
									{
										on: 'hover',
										style: {
											itemOpacity: 1
										}
									}
								]
							}
						]}
						theme={{
							tooltip: {
								container: {
									fontSize: '13px'
								}
							},
							labels: {
								textColor: '#555',
							}
						}}
					/>
				</Card>
			</div>
		);
	}
}

export default BarChart;
