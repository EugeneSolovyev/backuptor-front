import StatusConsoleActionType from './StatusConsole.action-types';
const initialState = {
	response: []
};

export default (state = _.clone(initialState), action) => {
	switch (action.type) {
		case StatusConsoleActionType.STATUS_CONSOLE_GET_SUCCESS:
			state.response = [ ...state.response, { success: action.payload } ];
			return { ...state };
		case StatusConsoleActionType.STATUS_CONSOLE_GET_ERROR:
			state.response = [ ...state.response, { error: 'Data was not fetched...' } ];
			return { ...state };
		case StatusConsoleActionType.STATUS_CONSOLE_DESTROY:
			state = initialState;
			return { ...state };
		default:
			return state;
	}
};
