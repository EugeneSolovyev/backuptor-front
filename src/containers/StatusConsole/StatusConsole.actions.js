import StatusConsoleActionType from './StatusConsole.action-types';
let count = 0;
const GetCurrentProcess = (company) => {
	count++;
	return {
		type: StatusConsoleActionType.STATUS_CONSOLE_GET_SUCCESS,
		payload: `fetching...${company.companyName} #${count}`,
	}
}
const UnmountStatusConsole = () => {
	count = 0;
	return {
		type: StatusConsoleActionType.STATUS_CONSOLE_DESTROY,
	}
}

export {
	GetCurrentProcess,
	UnmountStatusConsole,
}