import * as Actions from './StatusConsole.actions';
import StatusConsole from './StatusConsole.component';
import ReduxHOC from '../_ContainerWrapper';

const mapStateToProps = state => ({
	...state.StatusConsole,
});

export default ReduxHOC(StatusConsole, mapStateToProps, Actions);