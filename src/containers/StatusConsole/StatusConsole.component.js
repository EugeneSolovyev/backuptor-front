import React from 'react';
import { Drawer } from 'antd';
import './style.styl';

const StatusConsoleStyle = {
	backgroundColor: '#0D1F2D',
	width: 'inherit',
	height: '400px'
};

class StatusConsole extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			open: true
		};
		this.getProcess = this.getProcess.bind(this);
		this.closeConsole = this.closeConsole.bind(this);
	}

	componentDidMount() {
		this.getProcess();
		this.interval = setInterval(this.getProcess, 5000);
	}

	closeConsole() {
		this.setState((prev) => ({
			open: !prev.open
		}));
		this.props.closeConsole();
	}

	getProcess() {
		this.props.GetCurrentProcess(this.props.data);
	}

	componentWillUnmount() {
		clearInterval(this.interval);
		this.props.UnmountStatusConsole();
	}

	render() {
		let { response } = this.props;

		const data = response.map(
			(item, key) =>
				item.success ? (
					<p key={key} className="success">
						{item.success}
					</p>
				) : (
					<p key={key} className="error">
						{item.error}
					</p>
				)
		);

		return (
			<div className="status-console">
				<Drawer
					className="status-console__drawer"
					placement="bottom"
					width={'100%'}
					closable={true}
					onClose={this.closeConsole}
					style={StatusConsoleStyle}
					visible={this.state.open}
				>
					{data}
				</Drawer>
			</div>
		);
	}
}

export default StatusConsole;
