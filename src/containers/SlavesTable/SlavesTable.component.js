import React from 'react';
import {
	Table,
	Badge,
	Button,
	Icon,
} from 'antd';
import {
	Link
} from 'react-router-dom';

class SlavesTable extends React.PureComponent {
	constructor(props) {
		super(props);
		this.buildSlavesData = this.buildSlavesData.bind(this);
	}

	buildSlavesData(pureSlaves, pureCompanies) {
		return pureSlaves.map(slave => ({
			key: slave.id,
			name: slave.name,
			address: slave.publicAddress,
			companies: pureCompanies,
			uuid: slave.uuid,
		}));
	}

	render () {
		let slaves = this.buildSlavesData(this.props.slaves, this.props.companies);
		return (
			<div>
				<h3>Slaves</h3>
				<Table
					columns={SlaveTableHeader} 
					dataSource={slaves} 
					pagination={false}/>
			</div>
		);
	}
}

const SlaveTableHeader = [
	{
		title: 'Name',
		dataIndex: 'name',
		key: 'name',
	},
	{
		title: 'Address',
		dataIndex: 'address',
		key: 'address',
	},
	{
		title: 'Companies',
		dataIndex: 'companies',
		key: 'companies',
		render: companies => (
			<Badge 
				style={{
					backgroundColor: '#52c41a',
					color: '#ffffff',
				}}
				count={companies.length} />
		),
	},
	{
		title: 'Action',
		key: 'action',
		render: slave => (
				<Link 
					to={{
						pathname: `/slave/${slave.name.replace(/\s/g, '_')}`,
						state: {...slave}
					}}>
					<Button block icon='home'>Open {slave.name}</Button>
				</Link>
		),
	}
];

export default SlavesTable;