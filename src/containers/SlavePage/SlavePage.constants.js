const SlavePageActionsConstant = [
	{
		tooltip: 'Open Slave Settings',
		icon: 'setting',
	},
	{
		tooltip: 'Edit Slave',
		icon: 'edit',
	},
];

export {
	SlavePageActionsConstant,
}