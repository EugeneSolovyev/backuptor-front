import React from 'react';
import { Card, Icon, Row, Col } from 'antd';
import StatisticColumn from '../StatisticColumn';
import MasterSlaveEdit from '../MasterSlaveEdit';

class SlavePage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			clickEditable: false,
			...this.props.location.state
		};
		this.updateChart = this.updateChart.bind(this);
		this.toggleEditable = this.toggleEditable.bind(this);
	}

	componentDidMount() {
		this.updateChart();
	}

	updateChart() {
		let payload = {
			type: 0,
			uuid: this.state.uuid
		};
		this.props.GetCPU(payload);
		this.props.GetMEM(payload);
		this.props.GetDiskSpace(payload);
	}

	toggleEditable() {
		this.setState({
			clickEditable: !this.state.clickEditable
		});
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	render() {
		let { name, address, uuid, clickEditable } = this.state;

		return (
			<div className="backuptor-slave-profile">
				<Row gutter={16}>
					<Col lg={14}>
						<Card actions={[ <Icon type="edit" onClick={this.toggleEditable} /> ]}>
							<Card.Meta title={name} description={address} />
						</Card>
					</Col>
					<Col lg={10}>
						<StatisticColumn {...this.props} />
					</Col>
				</Row>
				{clickEditable && (
					<MasterSlaveEdit
						visible={clickEditable}
						type="slave"
						onClose={this.toggleEditable}
						entity={{ name, address, uuid }}
					/>
				)}
			</div>
		);
	}
}

export default SlavePage;
