import SlavePageActionTypes from './SlavePage.action-types';
import SharedActions from '../../shared';

const initialState = {
	CPU: [],
	InfoCPU: {},
	KeysCPU: [],
	MemoryKeys: [ 'total', 'used', 'free' ],
	MEMORY: [],
	DiskSpaceKeys: [ 'size', 'used', 'free' ],
	DiskSpaces: [],
	fetching: true,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case SlavePageActionTypes.GET_CPU_SUCCESS:
			SharedActions.updateStateWithFetchedCPU(state, action);

			return {
				...state,
			};
		case SlavePageActionTypes.GET_MEM_SUCCESS:
			SharedActions.updateStateWithFetchedMemory(action, state);
			return {
				...state
			};
		case SlavePageActionTypes.GET_DISK_SPACE_SUCCESS:
			SharedActions.updateStateWithFetchedDiskSpaces(action, state);
			return {
				...state
			};
		default:
			return state;
	}
};