import * as Actions from './SlavePage.actions';
import SlavePage from './SlavePage.component';
import ReduxHOC from '../_ContainerWrapper';

const mapStateToProps = (state) => ({
	...state.SlavePage,
});

export default ReduxHOC(SlavePage, mapStateToProps, Actions);