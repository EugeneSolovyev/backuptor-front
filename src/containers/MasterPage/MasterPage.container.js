import * as Actions from './MasterPage.actions';
import MasterPage from './MasterPage.component';
import ReduxHOC from '../_ContainerWrapper';

const mapStateToProps = (state) => ({
	...state.MasterPage,
});

export default ReduxHOC(MasterPage, mapStateToProps, Actions);