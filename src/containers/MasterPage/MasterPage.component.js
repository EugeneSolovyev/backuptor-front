import React from 'react';
import { Card, Icon, Tooltip, Row, Col, Spin, Table } from 'antd';

import StatisticColumn from '../StatisticColumn';
import CompanyTable from '../CompanyTable';
import MasterSlaveEdit from '../MasterSlaveEdit';
import ContainerMonitoring from '../ContainerMonitoring';

import './style.styl';

class MasterPage extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			clickEditable: false,
			...this.props.location.state,
			...this.props.companies
		};
		this.getCPU = this.getCPU.bind(this);
		this.toggleEditable = this.toggleEditable.bind(this);
	}

	componentDidMount() {
		this.getCPU();
		this.interval = setInterval(this.getCPU, 5000);
	}

	getCPU() {
		let payload = {
			type: 1,
			uuid: this.state.uuid
		};
		this.props.GetCPU(payload);
		this.props.GetMEM(payload);
		this.props.GetDiskSpace(payload);
	}

	toggleEditable() {
		this.setState({
			clickEditable: !this.state.clickEditable
		});
	}

	componentWillUnmount() {
		clearInterval(this.interval);
	}

	render() {
		let { name, address, uuid, clickEditable } = this.state;

		return (
			<div className="backuptor-master-profile">
				<Row gutter={16}>
					<Col xs={24} lg={14}>
						<Card
							className="backuptor-master-profile__info"
							actions={[ <Icon type="edit" onClick={this.toggleEditable} /> ]}
						>
							<Card.Meta title={name} description={address} />
						</Card>
						<Card className="backuptor-master-profile__companies">
							<Card.Meta title="Bonded Companies" />
							<CompanyTable companies={this.state.companies} />
						</Card>
						<Card className="backuptor-master-profile__companies">
							<ContainerMonitoring uuid={uuid} />
						</Card>
					</Col>
					<Col xs={24} lg={10}>
						<StatisticColumn {...this.props} />
					</Col>
				</Row>
				{clickEditable && <MasterSlaveEdit visible={clickEditable} type="master" onClose={this.toggleEditable} entity={{name, address, uuid}} />}
			</div>
		);
	}
}

export default MasterPage;
