import React from 'react';
import {Button} from 'antd';

const MasterPageActionsConstant = [
	{
		tooltip: 'Open Master Settings',
		icon: 'setting'
	},
	{
		tooltip: 'Edit Master',
		icon: 'edit'
	}
];

const BuildCompaniesTable = (CompaniesRaw) => {
	let data = [];
	let columns = [
		{
			title: 'Company Name',
			key: 'name',
			dataIndex: 'name'
		},
		{
			title: 'Company path',
			key: 'path',
			dataIndex: 'path'
		},
		{
			title: 'Docker DB name',
			key: 'dockerDbName',
			dataIndex: 'dockerDbName',
		},
		{
			title: 'Docker user',
			key: 'dockerDbUser',
			dataIndex: 'dockerDbUser'
		},
		{
			title: 'Docker Name',
			key: 'dockerName',
			dataIndex: 'dockerName'
		},
		{
			title: 'Port',
			key: 'dockerDbPort',
			dataIndex: 'dockerDbPort'
		},
		{
			title: 'Make Replication',
			key: 'replication',
			render: () => (
				<span>
					<Button>Backup</Button>
				</span>
			)
		}
	];

	data = CompaniesRaw.map((company) => ({
		key: company.id,
		name: company.companyName,
		path: company.companyPath,
		dockerDbName: company.dockerDBName,
		dockerDbUser: company.dockerDbUser,
		dockerName: company.dockerName,
		dockerDbPort: company.dockerDbPort
	}));

	return {
		data,
		columns
	};
};

export { MasterPageActionsConstant, BuildCompaniesTable };
