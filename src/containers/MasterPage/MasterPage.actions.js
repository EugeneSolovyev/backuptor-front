import MasterPageActionTypes from './MasterPage.action-types';
import {FetchingPost} from '../../API/api';
import API_URL from '../../API/api.constants';
import {message} from 'antd';

const GetCPU = (state) => {
	return dispatch => FetchingPost(API_URL.GetCPU, state)
		.then(({
			cpu_info,
			cpu_loads,
		}) => {
			let KeysCPU = [];
			let CurrentLoadedCPU = {};
			const UpdateTime = moment().format('hh:mm:ss');

			_.forIn(cpu_loads, (value, key) => {
				let CurrentKey = `Core#${key}`;
				if (key == -1) CurrentKey = 'Average';
				KeysCPU.push(CurrentKey);
				CurrentLoadedCPU[CurrentKey] = _.round(value, 2);
			})

			dispatch({
				type: MasterPageActionTypes.GET_CPU_SUCCESS,
				payload: {
					KeysCPU,
					CurrentLoadedCPU,
					cpu_info,
					UpdateTime,
				}
			})
		})
		.catch(error => {
			message.error(error.message, 2.5);
			dispatch({
				type: MasterPageActionTypes.GET_CPU_ERROR,
			})
		});
}

const GetMEM = (state) => {
	return dispatch => FetchingPost(API_URL.GetMemory, state)
		.then((response) => {
			dispatch({
				type: MasterPageActionTypes.GET_MEM_SUCCESS,
				payload: response,
			})
		})
		.catch(error => {
			message.error(error.message, 2.5);
			dispatch({
				type: MasterPageActionTypes.GET_MEM_ERROR,
			})
		});
}

const GetDiskSpace = (state) => {
	return dispatch => FetchingPost(API_URL.GetDiskSpace, state)
		.then(response => {
			let DiskSpaces = []
			_.forIn(response, (value, key) => {
				let data = value;
				data.path = key;
				DiskSpaces.push(data);
			})
			dispatch({
				type: MasterPageActionTypes.GET_DISK_SPACE_SUCCESS,
				payload: {DiskSpaces},
			})
		})
		.catch(error => {
			message.error(error.message, 2.5);
			dispatch({
				type: MasterPageActionTypes.GET_DISK_SPACE_ERROR,
			})
		});
}

export {
	GetCPU,
	GetMEM,
	GetDiskSpace,
}