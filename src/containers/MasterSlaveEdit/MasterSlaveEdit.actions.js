import MasterSlaveEditActionTypes from './MasterSlaveEdit.action-types';
import { FetchingPost } from '../../API/api';
import API_URL from '../../API/api.constants';

const UpdateMasterSlave = (state, type) => {
	let Method = type === 'slave' ? API_URL.UpdateSlave : API_URL.UpdateMaster;

	return dispatch =>
		FetchingPost(Method, state)
			.then(() => {
				dispatch({
					type: MasterSlaveEditActionTypes.MASTER_SLAVE_SUCCESSFULLY_UPDATED,
				})
			})
			.catch(() => {
				dispatch({
					type: MasterSlaveEditActionTypes.MASTER_SLAVE_FAILFULLY_UPDATED,
				})
			});
};

export {
	UpdateMasterSlave,
};
