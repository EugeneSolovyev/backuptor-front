import ReduxHOC from '../_ContainerWrapper';
import * as Actions from './MasterSlaveEdit.actions';
import MasterSlaveEdit from './MasterSlaveEdit.component';

const mapStateToProps = state => ({
	
});

export default ReduxHOC(MasterSlaveEdit, mapStateToProps, Actions);