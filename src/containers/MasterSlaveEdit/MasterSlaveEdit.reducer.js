import MasterSlaveEditActionTypes from './MasterSlaveEdit.action-types';

const initialState = {};

export default (state = initialState, action) => {
	switch(action.type) {
		default:
			return state;
	}
}