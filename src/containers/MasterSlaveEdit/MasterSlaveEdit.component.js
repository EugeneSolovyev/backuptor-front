import React from 'react';
import {Modal, Input} from 'antd';

class MasterSlaveEdit extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			...this.props.entity,
		};
		this.handleOk = this.handleOk.bind(this);
		this.handleCancel = this.handleCancel.bind(this);
		this.updateField = this.updateField.bind(this);
	}

	updateField(e) {
		this.setState({
			[e.target.id]: e.target.value,
		})
	}

	handleOk() {
		let {name, uuid, address} = this.state;
		this.props.UpdateMasterSlave({
			name,
			uuid,
			publicAddress: address,
		}, this.props.type)
		this.props.onClose();
	}

	handleCancel() {
		this.props.onClose();
	}


	render() {
		let {
			type,
			visible,
		} = this.props;
		return (
			<div>
				<Modal 
					title={`Edit ${type}`}
					onOk={this.handleOk}
					onCancel={this.handleCancel} 
					visible={visible}>
					<Input id='name' onChange={this.updateField} value={this.state.name}/>
					<Input id='address' onChange={this.updateField} value={this.state.address} />
				</Modal>
			</div>
		);
	}
}

export default MasterSlaveEdit;