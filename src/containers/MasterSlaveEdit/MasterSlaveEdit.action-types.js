const MasterSlaveEditActionTypes = {
	MASTER_SLAVE_SUCCESSFULLY_UPDATED: 'MASTER_SLAVE_SUCCESSFULLY_UPDATED',
	MASTER_SLAVE_FAILFULLY_UPDATED: 'MASTER_SLAVE_FAILFULLY_UPDATED',
}

export default MasterSlaveEditActionTypes;