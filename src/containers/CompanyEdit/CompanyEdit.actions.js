import CompanyEditActionTypes from './CompanyEdit.action-types';
import {FetchingPost} from '../../API/api';
import API_URL from '../../API/api.constants';

const UpdateCompany = (state) => {
	return dispatch => FetchingPost(API_URL.UpdateCompany, {
		masterCompanyUUID: state.masterCompanyUUID,
		companyName: state.companyName,
		companyPath: state.companyPath,
		containerName: state.dockerName,
		containerDbName: state.dockerDBName,
		containerDbUser: state.dockerDbUser,
		containerDbPassword: state.dockerDbPassword,
		containerDbRootPassword: state.dockerDbRootPassword,
		containerDbPort: state.dockerDbPort,
	})
		.then(() => {
			dispatch({
				type: CompanyEditActionTypes.COMPANY_EDIT_SUCCESS,
				payload: state,
			})
		})
		.catch(() => {
			dispatch({
				type: CompanyEditActionTypes.COMPANY_EDIT_ERROR,
			})
		})
};

export {
	UpdateCompany,
}