const CompanyEditActionTypes = {
	COMPANY_EDIT_SUCCESS: 'COMPANY_EDIT_SUCCESS',
	COMPANY_EDIT_ERROR: 'COMPANY_EDIT_ERROR',
};

export default CompanyEditActionTypes;