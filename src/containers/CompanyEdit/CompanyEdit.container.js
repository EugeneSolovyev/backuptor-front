import ReduxHOC from '../_ContainerWrapper';
import * as Actions from './CompanyEdit.actions';
import CompanyEdit from './CompanyEdit.component';

const mapStateToProps = state => ({
	...state.CompanyEdit,
});

export default ReduxHOC(CompanyEdit, mapStateToProps, Actions);