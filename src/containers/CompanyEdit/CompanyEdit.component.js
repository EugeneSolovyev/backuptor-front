import React from 'react';
import { Modal, Input, Icon } from 'antd';

import './style.styl';

class CompanyEdit extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			...this.props.company,
		};
		this.handleOk = this.handleOk.bind(this);
		this.handleCancel = this.handleCancel.bind(this);
		this.updateField = this.updateField.bind(this);
	}

	updateField(e) {
		this.setState({
			[e.target.name]: e.target.value,
		});
	}

	handleCancel() {
		this.props.ToggleModal();
	}

	handleOk() {
		this.props.UpdateCompany(this.state);
		this.props.ToggleModal();
	}

	render() {
		return (
			<div className='company-edit-modal'>
				<Modal 
					title="Edit Company" 
					width={780}
					onOk={this.handleOk}
					onCancel={this.handleCancel}
					visible={this.props.visible}>
					<Input 
						className='company-edit-modal__input'
						placeholder='Company Name' 
						prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
						name='companyName' 
						onChange={this.updateField}
						value={this.state.companyName} />
					<Input 
						className='company-edit-modal__input'
						placeholder='Company Path' 
						prefix={<Icon type="folder" style={{ color: 'rgba(0,0,0,.25)' }} />}
						name='companyPath' 
						onChange={this.updateField}
						value={this.state.companyPath} />
					<Input 
						className='company-edit-modal__input'
						placeholder='Docker DB Name' 
						prefix={<Icon type="home" style={{ color: 'rgba(0,0,0,.25)' }} />}
						name='dockerDBName' 
						onChange={this.updateField}
						value={this.state.dockerDBName} />
					<Input 
						className='company-edit-modal__input'
						placeholder='Docker DB Password' 
						prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />}
						name='dockerDbPassword' 
						type='password'
						onChange={this.updateField}
						value={this.state.dockerDbPassword} />
					<Input 
						className='company-edit-modal__input'
						placeholder='Docker DB Port' 
						prefix={<Icon type="link" style={{ color: 'rgba(0,0,0,.25)' }} />}
						name='dockerDbPort' 
						onChange={this.updateField}
						value={this.state.dockerDbPort} />
					<Input 
						className='company-edit-modal__input'
						placeholder='Docker DB User' 
						prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
						name='dockerDbUser' 
						onChange={this.updateField}
						value={this.state.dockerDbUser} />
					<Input 
						className='company-edit-modal__input'
						placeholder='Docker Name' 
						prefix={<Icon type="home" style={{ color: 'rgba(0,0,0,.25)' }} />}
						name='dockerName' 
						onChange={this.updateField}
						value={this.state.dockerName} />
				</Modal>
			</div>
		);
	}
}

export default CompanyEdit;
