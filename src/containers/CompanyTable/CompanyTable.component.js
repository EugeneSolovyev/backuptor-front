import React from 'react';
import { Button, Table, Divider } from 'antd';

import { BaseCompaniesFieldFortable } from './CompanyTable.constants';
import StatusConsole from '../StatusConsole';
import CompanyEdit from '../CompanyEdit';

class CompanyTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			clickedConsole: false,
			clickedEditable: false,
		};
		this.OpenConsole = this.OpenConsole.bind(this);
		this.BuildCompaniesTable = this.BuildCompaniesTable.bind(this);
		this.ToggleModal = this.ToggleModal.bind(this);
	}

	BuildCompaniesTable(CompaniesRaw) {
		let data = [];
		let columns = [
			...BaseCompaniesFieldFortable,
			{
				title: 'Make Replication',
				key: 'replication',
				render: (company) => (
					<span>
						<Button onClick={() => this.OpenConsole(company)}>Backup</Button>
						<Divider type='vertical' />
						<Button onClick={() => this.ToggleModal(company)}>Edit</Button>
					</span>
				)
			}
		];

		data = CompaniesRaw.map((company) => ({
			key: company.id,
			...company
		}));

		return {
			data,
			columns
		};
	}

	OpenConsole(company) {
		this.setState({
			clickedConsole: !this.state.clickedConsole,
			company
		});
	}

	ToggleModal(company) {
		this.setState({
			clickedEditable: !this.state.clickedEditable,
			company,
		})
	}

	render() {
		let { data, columns } = this.BuildCompaniesTable(this.props.companies);

		return (
			<div>
				<Table
					className="backuptor-master-profile__table"
					dataSource={data}
					columns={columns}
					scroll={{ x: true }}
				/>
				{this.state.clickedConsole && <StatusConsole data={this.state.company} closeConsole={this.OpenConsole} />}
				{this.state.clickedEditable && <CompanyEdit visible={this.state.clickedEditable} company={this.state.company} ToggleModal={this.ToggleModal} />}
			</div>
		);
	}
}

export default CompanyTable;
