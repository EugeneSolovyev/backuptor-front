const BaseCompaniesFieldFortable = [
	{
		title: 'Company Name',
		key: 'companyName',
		dataIndex: 'companyName'
	},
	{
		title: 'Company Path',
		key: 'companyPath',
		dataIndex: 'companyPath'
	},
	{
		title: 'DB Name',
		key: 'dockerDBName',
		dataIndex: 'dockerDBName'
	},
	{
		title: 'DB User',
		key: 'dockerDbUser',
		dataIndex: 'dockerDbUser'
	},
	{
		title: 'Docker Name',
		key: 'dockerName',
		dataIndex: 'dockerName'
	},
	{
		title: 'Port',
		key: 'dockerDbPort',
		dataIndex: 'dockerDbPort'
	},
];

export {
	BaseCompaniesFieldFortable,
};