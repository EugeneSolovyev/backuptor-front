import React from 'react';
import {
	Table,
	Badge,
	Button,
} from 'antd';
import {
	Link
} from 'react-router-dom';

class MastersTable extends React.PureComponent {
	constructor(props) {
		super(props);
		this.buildMastersData = this.buildMastersData.bind(this);
	}

	buildMastersData(pureMasters, pureCompanies) {
		return pureMasters.map(master => ({
			key: master.id,
			name: master.name,
			address: master.publicAddress,
			uuid: master.uuid,
			companies: pureCompanies,
		}))
	}

	render() {
		let masters = this.buildMastersData(this.props.masters, this.props.companies);
		
		return (
			<div className='backuptor-master-table'>
				<h3>Masters</h3>
				<Table 
					columns={MasterTableHeader} 
					dataSource={masters}
					pagination={false} />
			</div>
		);
	}
}

const MasterTableHeader = [
	{
		title: 'Name',
		dataIndex: 'name',
		key: 'name',
	},
	{
		title: 'Address',
		dataIndex: 'address',
		key: 'address',
	},
	{
		title: 'Companies',
		dataIndex: 'companies',
		key: 'companies',
		render: companies => (
			<Badge 
				style={{
					backgroundColor: '#52c41a',
					color: '#ffffff',
				}}
				count={companies.length} />
		),
	},
	{
		title: 'Action',
		key: 'action',
		render: master => (
			<Link 
				to={{
					pathname: `/master/${master.name.replace(/\s/g, '_')}`,
					state: {...master}
				}}>
				<Button 
					block 
					icon='home'>
						Open {master.name}
				</Button>
			</Link>
		),
	}
];

export default MastersTable;