import React from 'react';

import HeaderComponent from '../Header';
import MenuComponent from '../Menu';

class Layout extends React.PureComponent {
	componentDidMount() {
		this.props.GetAll();
	}

	render() {
		let { menuOpen, ToggleDrawer } = this.props;
		return (
			<div>
				<HeaderComponent ToggleDrawer={ToggleDrawer} />
				<MenuComponent menuOpen={menuOpen} ToggleDrawer={ToggleDrawer} />
			</div>
		);
	}
}

export default Layout;
