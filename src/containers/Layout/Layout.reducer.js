import LayoutActionTypes from './Layout.action-types';

const initialState = {
	menuOpen: false,
};

export default (state = initialState, action) => {
	switch (action.type) {
		case LayoutActionTypes.menuOpen:
			state.menuOpen = !state.menuOpen;
			return {...state};
		case LayoutActionTypes.ChangePage:
		default:
			return state;
	}
}