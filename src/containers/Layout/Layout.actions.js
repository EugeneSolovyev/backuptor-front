import API from '../../API/api';
import LayoutActionTypes from './Layout.action-types';

const ChangePage = (state) => {
	return {
		type: LayoutActionTypes.ChangePage,
		...state
	}
};

const ToggleDrawer = () => {
	return {
		type: LayoutActionTypes.menuOpen,
	}
}

export {
	ChangePage,
	ToggleDrawer,
};