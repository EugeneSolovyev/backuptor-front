import * as Actions from './Layout.actions';
import * as HomeActions from '../Home/Home.actions';
import Layout from './Layout.component';
import ReduxHOC from '../_ContainerWrapper';

const mapStateToProps = (state) => ({
	...state.Layout,
});

export default ReduxHOC(Layout, mapStateToProps, {
	...Actions,
	...HomeActions
});
