import React from 'react';
import { Spin } from 'antd';
import BarChart from '../BarChart';
import StreamChart from '../PageChart';

const StatisticColumn = (props) => {
	let { fetching, InfoCPU: {cpuName}, KeysCPU, CPU, MEMORY, MemoryKeys, DiskSpaces, DiskSpaceKeys } = props;

	return !fetching ? (
		<div className='StatisticSolumn'>
			<StreamChart title={`CPU: ${cpuName}`} Keys={KeysCPU} chartData={CPU} />
			<BarChart title="Memory" chartData={MEMORY} Keys={MemoryKeys} legendType="%" />
			<BarChart title="Disk Space" chartData={DiskSpaces} Keys={DiskSpaceKeys} legendType="%" />
		</div>
	) : (
		<Spin
			style={{
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				height: 150
			}}
			size="large"
		/>
	);
};

export default StatisticColumn;
