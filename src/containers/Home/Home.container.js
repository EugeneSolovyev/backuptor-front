import * as Actions from './Home.actions';
import Home from './Home.component';
import ReduxHOC from '../_ContainerWrapper';

const mapStateToProps = (state) => ({
	...state.Home,
});

export default ReduxHOC(Home, mapStateToProps, Actions);