import HomeActionTypes from './Home.action-types';
import {FetchingGet} from '../../API/api';
import API_URL from '../../API/api.constants';
import {message} from 'antd';

const GetAll = () => {
	return dispatch => FetchingGet(API_URL.GetAllData)
		.then(({
			slaves,
			masters,
			companies
		}) => {
			dispatch({
				type: HomeActionTypes.GET_ALL_SUCCESSFUL,
				payload: {
					slaves,
					masters,
					companies
				}
			})
		})
		.catch(error => {
			message.error(error.message, 2.5);
			dispatch({
				type: HomeActionTypes.GET_ALL_ERROR,
			});
		})
}

export {
	GetAll,
}