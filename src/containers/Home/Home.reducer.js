import HomeActionTypes from './Home.action-types';

const initialState = {
	slaves: [],
	masters: [],
	companies: []
};

export default (state = initialState, action) => {
	switch (action.type) {
		case HomeActionTypes.GET_ALL_SUCCESSFUL:
			state = action.payload;
			return state;
		default:
			return state;
	}
};
