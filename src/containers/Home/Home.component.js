import React from 'react';
import { Row, Col } from 'antd';
import MastersTable from '../MastersTable';
import SlavesTable from '../SlavesTable';

class Home extends React.Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		this.props.GetAll();
	}

	render() {
		let { masters, slaves, companies } = this.props;

		return (
			<Row gutter={16}>
				<Col lg={12}>
					<MastersTable masters={masters} companies={companies} />
				</Col>
				<Col lg={12}>
					<SlavesTable slaves={slaves} companies={companies} />
				</Col>
			</Row>
		);
	}
}

export default Home;
