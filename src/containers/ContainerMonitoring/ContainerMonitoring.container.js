import ReduxHOC from '../_ContainerWrapper';

import * as Actions from './ContainerMonitoring.actions';
import ContainerMonitoring from './ContainerMonitoring.component';

const mapStateToProps = state => ({
	...state.ContainerMonitoring,
});

export default ReduxHOC(ContainerMonitoring, mapStateToProps, Actions);