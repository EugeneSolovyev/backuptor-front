import ContainerMonitoring from './ContainerMonitoring.action-types';
import { FetchingGet } from '../../API/api';
import API_URL from '../../API/api.constants';

const FetchContainerMonitoringData = (state) => {

	return dispatch => FetchingGet(API_URL.DockersStatus, state)
		.then(response => {
		})
};

export { FetchContainerMonitoringData };
