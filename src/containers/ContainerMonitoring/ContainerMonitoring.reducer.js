import ContainerMonitoring from './ContainerMonitoring.action-types';

const initialState = {};

export default (state = initialState, action) => {
	switch (action.type) {
		case ContainerMonitoring.CONTAINER_INFO_SUCCESSFULLY_FETCHED:
		case ContainerMonitoring.CONTAINER_INFO_FAILFULLY_FETCHED:
		default:
			return state;
	}
}