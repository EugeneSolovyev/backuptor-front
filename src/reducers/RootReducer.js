import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import Layout from '../containers/Layout/Layout.reducer';
import Master from '../containers/Master/Master.reducer';
import Slave from '../containers/Slave/Slave.reducer';
import Company from '../containers/Company/Company.reducer';
import Home from '../containers/Home/Home.reducer';
import MasterPage from '../containers/MasterPage/MasterPage.reducer';
import SlavePage from '../containers/SlavePage/SlavePage.reducer';
import StatusConsole from '../containers/StatusConsole/StatusConsole.reducer';
import CompanyEdit from '../containers/CompanyEdit/CompanyEdit.reducer';
import MasterSlaveEdit from '../containers/MasterSlaveEdit/MasterSlaveEdit.reducer';
import ContainerMonitoring from '../containers/ContainerMonitoring/ContainerMonitoring.reducer';

export default combineReducers({
	router: routerReducer,
	Layout,
	Master,
	Slave,
	Company,
	Home,
	MasterPage,
	SlavePage,
	StatusConsole,
	CompanyEdit,
	MasterSlaveEdit,
	ContainerMonitoring
	//.. other reducers
});
