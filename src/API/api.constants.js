const API_URL = {
	RegisterCompany: 'registerCompany',
	RegisterMaster: 'registerMaster',
	RegisterSlave: 'registerSlave',
	GetAllData: 'getAllData',
	GetCPU: 'getCPU',
	GetMemory: 'getMemory',
	GetDiskSpace: 'getDiskSpace',
	UpdateCompany: 'updateMasterCompany',
	UpdateMaster: 'updateMaster',
	UpdateSlave: 'updateSlave',
	DockersStatus: 'dockersStatus'
};

export default API_URL;