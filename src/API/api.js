const _handleError = (response) => {
	if (response.status !== 200) {
		throw Error(`Status ${response.status}: ${response.url}`);
	}
	return response.json();
};

const _getUrlWithParams = (url, params) => {
	let ret = [];
	for (let key in params) {
		ret.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
	}
	if (ret.length) return `${url}?${ret.join('&')}`;
	return url;
};

const headers = new Headers({
	'Content-Type': 'application/json',
	'Access-Control-Request-Headers': '*'
});

const FetchingPost = (url, data) => {
	return fetch(`${process.env.SERVER_API}${url}`, {
		method: 'POST',
		headers,
		body: JSON.stringify(data)
	}).then(_handleError);
};

const FetchingGet = (url, params) => {
	const URL = _getUrlWithParams(url, params);
	return fetch(`${process.env.SERVER_API}${URL}`, {
		method: 'GET',
		headers
	}).then(_handleError);
};

export { headers, FetchingPost, FetchingGet };
