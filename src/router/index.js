import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';

import Layout from '../containers/Layout';
import Home from '../containers/Home';
import Master from '../containers/Master';
import Slave from '../containers/Slave';
import Company from '../containers/Company';
import MasterPage from '../containers/MasterPage';
import SlavePage from '../containers/SlavePage';

class App extends React.Component {
	render() {
		return (
			<div>
				<Layout />
				<Switch>
					<div className="backuptor-root">
						<Route exact path="/" component={Home} />
						<Route path="/register-master" component={Master} />
						<Route path="/register-slave" component={Slave} />
						<Route path="/register-company" component={Company} />
						<Route path="/master/:company" component={MasterPage} />
						<Route path="/slave/:company" component={SlavePage} />
					</div>
				</Switch>
			</div>
		);
	}
}

export default App;
