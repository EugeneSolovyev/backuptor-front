const _MEMORY_COLORS = {
	free: '#FF1493',
	used: '#7FFF00',
	total: '#2980b9',
	size: '#2980b9'
};

const _HandleMemory = (RawMemory) => {
	let result = {};
	_.forIn(RawMemory, (value, key) => {
		if (key === 'total') return;
		let percent = value / RawMemory.total * 100;
		result[key] = _.round(percent);
		result[`${key}Color`] = _MEMORY_COLORS[key];
    result['total'] = `${_.round(RawMemory.total, 2)} GB`;
	});
	return result;
};

const _HandleDiskSpaces = (RawDisk) => {
	let result = {};
	const FullSize = +RawDisk.free + +RawDisk.used;
	_.forIn(RawDisk, (value, key) => {
		if (key === 'size' || key === 'device' || key === 'usedPercent') return;

		if (Number(value)) {
      let percent = +value / FullSize * 100;
      result[key] = _.round(percent);
      result[`${key}Color`] = _MEMORY_COLORS[key];
		}
		else {
      result[key] = value;
		}

		result['total'] = `${_.round(RawDisk.size / 1024, 3)} GB`;
	});

	return result;
};

const updateStateWithFetchedMemory = (action, state) => {
	let { memory, swap: SWAP } = action.payload;
	let rom = _HandleMemory(memory);
	let swap = _HandleMemory(SWAP);
	state.MEMORY.push(
		{
			name: 'ROM',
			...rom
		},
		{
			name: 'Swap',
			...swap
		}
	);
	if (state.MEMORY.length === 4) state.MEMORY.splice(0, 2);
};

const updateStateWithFetchedCPU = (state, action) => {
	state.fetching = false;
	let { KeysCPU, CurrentLoadedCPU, cpu_info } = action.payload;
	if (_.isEmpty(state.KeysCPU)) state.KeysCPU = KeysCPU;
	if (_.isEmpty(state.InfoCPU)) state.InfoCPU = cpu_info;
	if (state.CPU.length === 11) state.CPU.splice(0, 1);
	state.CPU.push(CurrentLoadedCPU);
};

const updateStateWithFetchedDiskSpaces = (action, state) => {
	let { DiskSpaces } = action.payload;
	let diskSpaces = [];
	_.forEach(DiskSpaces, (disk) => {
		diskSpaces.push({
			name: disk.path,
			..._HandleDiskSpaces(disk)
		});
	});
	state.DiskSpaces = diskSpaces;
};

export { updateStateWithFetchedMemory, updateStateWithFetchedCPU, updateStateWithFetchedDiskSpaces };
