import React from 'react';
import {
	render
} from 'react-dom';
import {
	Provider,
} from 'react-redux';
import {
	ConnectedRouter
} from 'react-router-redux';
import store, {
	history
} from './store/store';
import App from './router';
import 'sanitize.css/sanitize.css';
import 'normalize.css/normalize.css';
import './content/general-styles/general-styles.styl';

import registerServiceWorker from './registerServiceWorker';

render(
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<div>
				<App/>
			</div>
		</ConnectedRouter>
	</Provider>,
	document.querySelector('#root')
);

// registerServiceWorker();