module.exports = env => {
  console.log(env);
  const CLI_PARAMS = process.argv;
  global.NODE_ENV = env || 'production';
  global.isProduction = global.NODE_ENV === 'production';
  let targets = [];
  global.rootPath = __dirname;

  if (~CLI_PARAMS.indexOf('--main')) {
    targets.push(require('./webpack_configs/config'));
  }

  return targets;
};