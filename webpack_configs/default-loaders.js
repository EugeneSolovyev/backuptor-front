const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const CSS_LOADERS = [
  'style-loader',
  MiniCssExtractPlugin.loader,
  {
    loader: 'css-loader',
    options: { minimize: true }
  },
  {
    loader: 'postcss-loader',
    options: {
      sourceMap: true,
      plugins: () => [
        require('autoprefixer')
      ]
    }
  },
  'resolve-url-loader',
];

let STYLUS_LOADER = CSS_LOADERS.concat([{
    loader: 'stylus-loader'
}]);

module.exports = [
  {
    test: /\.(js|jsx|mjs)$/,
    exclude: /node_modules/,
    use: {
      loader: 'babel-loader',
    }
  },
  {
    test: /\.jpg$/,
    'use': ['file-loader']
  },
  {
    test: /\.png$/,
    'use': ['url-loader?mimetype=image/png']
  },
  {
    test: /\.gif$/,
    'use': ['url-loader?mimetype=image/gif']
  },
  {
    test: /\.css$/,
    use: CSS_LOADERS
  },
  {
    test: /\.styl$/,
    use: STYLUS_LOADER
  },
  {
    test: /\.(eot|ttf|woff|woff2)$/,
    use: [
      {
        loader: 'file-loader'
      }
    ]
  },
  {
    test: /\.svg$/,
    exclude: /node_modules/,
    use: [
      {
        loader: 'url-loader',
        options: {
          limit: 100000
        }
      }
    ]
  }
];