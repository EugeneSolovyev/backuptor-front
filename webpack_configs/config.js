const path = require('path');
const DEFAULT_LOADERS = require('./default-loaders');
const DEFAULT_PLUGINS = require('./default-plugins');
const PUBLIC_PATH = '/';

module.exports = {
    context: global.rootPath,
    watchOptions: {
        aggregateTimeout: 100,
    },
    devServer: {
        //contentBase: path.join(global.rootPath, 'bundle'),
        compress: true,
        port: 9000,
    },
    devtool: global.isProduction
      ? false
      : 'cheap-module-eval-source-map',
    entry: {
        'source': path.join(global.rootPath, 'src', 'index.js')
    },
    output: {
        filename: '[name].js',
        path: path.join(global.rootPath, 'bundle'),
        publicPath: PUBLIC_PATH
    },
    module: {
        rules: DEFAULT_LOADERS,
    },
	devServer: {
		historyApiFallback: true,
	},
    plugins: DEFAULT_PLUGINS,
};