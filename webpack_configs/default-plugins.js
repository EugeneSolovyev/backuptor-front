const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const ManifestPlugin = require('webpack-manifest-plugin');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');

const webpack = require('webpack');

let defaultPlugins = [
	new MiniCssExtractPlugin({
		filename: '[name].css'
	}),
	new ProgressBarPlugin(),
	new HtmlWebpackPlugin({
		template: './src/index.html',
		filename: '../bundle/index.html',
		inject: false
	}),
	new webpack.ProvidePlugin({
		$: 'jquery',
		jQuery: 'jquery',
		_: 'lodash',
		moment: 'moment',
	}),
	new ManifestPlugin({
		fileName: 'asset-manifest.json',
	}),
	new SWPrecacheWebpackPlugin({
		dontCacheBustUrlsMatching: /\.\w{8}\./,
		filename: '../service-worker.js',
		logger(message) {
			if (message.indexOf('Total precache size is') === 0) {
				return;
			}
			console.log(message);
		},
		minify: true,
		navigateFallback: '/index.html',
		staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/],
	}),
	new CopyWebpackPlugin([
		{ from: 'src/pwa' },
	]),
	new Dotenv(),
];

module.exports = defaultPlugins;